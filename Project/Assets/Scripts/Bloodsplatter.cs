﻿using UnityEngine;
using System.Collections;

public class Bloodsplatter : MonoBehaviour {

    public Sprite[] splatters; //sprite array to hold blood splatter sprites

    //when blood splatter is enabled randomise sprite used
    void OnEnable()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = splatters[Random.Range(0,7)];
    }
}
