﻿using UnityEngine;
using System.Collections;

public class Bloodspray : MonoBehaviour {

    public float destroyTime = 0.7f;

	//invoke the gameobject's death
	void Start () {
        Invoke("Die", destroyTime);
	}
	
    //destroy the gameobject
	void Die()
    {
        Destroy(gameObject);
    }
}
