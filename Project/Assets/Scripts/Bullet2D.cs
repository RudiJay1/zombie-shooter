﻿using UnityEngine;
using System.Collections;

public class Bullet2D : MonoBehaviour {

    public float speed = 5.0f;  //the speed the bullet travels at
    public float destroyTime = 0.7f;    //how long before the bullet is destroyed

    private Rigidbody2D rb2d;

    //instantiate rigidbody
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    //when the bullet is fired, set a timer to kill it
	void OnEnable()
    {
        Invoke("Die", destroyTime);
	}

    //kills the bullet
    void Die()
    {
        gameObject.SetActive(false);
    }

    //when the bullet has been killed, stop the timer
    void OnDisable()
    {
        CancelInvoke("Die");
    }
	
    //move the bullet at an interval
	void FixedUpdate()
    {
        rb2d.velocity = transform.up * speed;
	}
}
