﻿using UnityEngine;
using System.Collections;

public class BulletHit2D : MonoBehaviour {

    public int damage = 1;


    //when the bullet collides with another game object, if that object
    //has a damage tag, tell it to take damage and then kill the bullet
    //if it has an invisible tag, ignore it
    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Invisible"))
        {
            damage = Random.Range(1, 4);

            if (other.gameObject.CompareTag("Zombie"))
            {
                other.SendMessage("TakeDamage", damage);
            }
            else if (other.gameObject.CompareTag("Hero"))
            {
                other.SendMessage("TakeDamage", "bullet");
            }

            gameObject.SetActive(false);
        }
    }
}
