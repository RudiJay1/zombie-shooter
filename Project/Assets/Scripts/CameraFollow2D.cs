﻿using UnityEngine;
using System.Collections;

public class CameraFollow2D : MonoBehaviour {

    public float smoothing = 5.0f;  //how smooth the camera movement is
    public float lookDistance = 1f; //the distance the camera looks further in the direction the player is facing

    void Start()
    {
        transform.position = new Vector3(GameManager.instance.cameraTarget.position.x, GameManager.instance.cameraTarget.position.y - 7.10f, transform.position.z);
    }

    //find the new position to move the camera to
    //position the camera further in the direction the hero is looking
    //move it there using smoothing
	void FixedUpdate()
    {
        Vector3 newPos = new Vector3(GameManager.instance.cameraTarget.position.x, GameManager.instance.cameraTarget.position.y, transform.position.z);
        newPos = newPos + (GameManager.instance.cameraTarget.up.normalized * lookDistance);
        transform.position = Vector3.Lerp( transform.position, newPos, ( smoothing * 0.001f ) );
    }
}
