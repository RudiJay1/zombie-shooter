﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public static GameManager instance; //an instance of the game manager that can be used to call any variables

    public Transform cameraTarget; //the target for the camera to follow, initialised to the hero

    public float heroSpeed;  //the speed of the hero

    public bool unnoticed; //whether the hero has been noticed by enemies on this level
    public bool enemiesCleared; //whether or not all the enemies on a level are dead
    public float startTime; //the time the level was started at, used to work out time taken
    public float timeBeforePause; //the time taken to complete a level, stored in case the game is paused

    public int chances; //number of times the hero can be hit by the enemy before being bitten

    public bool heroBitten; //whether the hero is currently already bitten
    public bool heroDead; //whether the hero has died
    public bool allowHeroControl; //whether the player can currently control the hero freely
    public bool showingResultsScreen; //whether the player is viewing the results screen

    public float invincibilityTime; //length of time the hero is invincible after taking damage
    public bool invincible; //whether the hero is currently invincible

    public bool shakeScreen; //whether or not to shake the screen

    public string weaponEquipped; //the weapon currently equipped by hero

    public float fireDelay; //the time it takes for a gun to be able to fire again
    public float reloadTime; //the time it takes to reload a gun
    public float spread; //the amount of variation from the straightforward path a bullet takes
    public float gunNoiseRadius; //the radius zombies will hear your gun from

    public int ammoCapacity; //the number of shots a weapon has before it must be reloaded
    public int loadedAmmo; //the number of shots the weapon has remaining before it must be reloaded
    public int storedAmmo; //the number of shots the hero is carrying in total

    //sets the current instance of the game manager to the instance to be used by other components
    void Awake()
    {
        instance = this;
    }

    //reset timescale if scene is reloaded after results screen
    //initialise variables so they reset when the scene resets
    void Start()
    {
        startTime = Time.realtimeSinceStartup;
        
        Time.timeScale = 1;
        
        cameraTarget = GameObject.Find("Hero").transform;

        unnoticed = true;
        enemiesCleared = false;

        showingResultsScreen = false;

        heroSpeed = 3f;

        chances = 3;

        heroBitten = false;
        heroDead = false;
        allowHeroControl = true;

        invincibilityTime = 2f;
        invincible = false;

        shakeScreen = false;

        weaponEquipped = "pistol";

        if (weaponEquipped == "pistol")
        {
            fireDelay = 0.5f;
            reloadTime = 1.0f;
            spread = 7f;
            gunNoiseRadius = 5f;

            ammoCapacity = 8;
            loadedAmmo = ammoCapacity;
            storedAmmo = 12;
        }
        else if (weaponEquipped == "fullauto")
        {
            fireDelay = 0.2f;
            reloadTime = 1.0f;
            spread = 10f;
            gunNoiseRadius = 10f;
            ammoCapacity = 30;
            loadedAmmo = ammoCapacity;
            storedAmmo = 220;
        }
    }

    public void PickUpPistol()
    {
        weaponEquipped = "pistol";
        fireDelay = 0.5f;
        reloadTime = 1.0f;
        spread = 7f;
        gunNoiseRadius = 5f;

        ammoCapacity = 8;
        loadedAmmo = ammoCapacity;
        storedAmmo = 12;
    }

    public void PickUpFullAuto()
    {
        weaponEquipped = "fullauto";
        fireDelay = 0.15f;
        reloadTime = 1.0f;
        spread = 10f;
        gunNoiseRadius = 10f;
        ammoCapacity = 30;
        loadedAmmo = ammoCapacity;
        storedAmmo = 220;
    }

    //restarts the level upon death
    void RestartLevel()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
