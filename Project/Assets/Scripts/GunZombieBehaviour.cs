﻿using UnityEngine;
using System.Collections;

public class GunZombieBehaviour : MonoBehaviour {

    public Transform weapon; //the weapon the bullets spawn from

    public Transform target; //the target to rotate towards

    public float smoothing = 5.0f; //the smoothness the zombie rotates towards its target at
    public float adjustmentAngle = 270f; //angle to rotate sprite so it is facing correct way

    private Animator anim; //the animator for the zombie

    private AudioSource audio; //audio source
    public AudioClip[] attackSounds; //array to hold attack sound effects
    public AudioClip[] groanSounds; //array to hold groan sound effects
    public AudioClip[] hitSounds; //array to hold hit sound effects
    public AudioClip[] squelchSounds; //array to hold squelch sound effects
    public AudioClip[] gunshotSounds; //array to hold gunshot sound effects
    public AudioClip reloadSound; //reload sound effect
    private bool playAttackSounds = true;

    public GameObject bloodsprayPrefab; //the animated sprite for the bloodspray

    public int health = 5;  //number of lives the zombie has

    public bool isDead = false; //whether the zombie is dead again or not
    public bool spottedHero = false; //whether the zombie has seen the hero or not

    public bool canFire = true;
    public float fireDelay = 0.2f;
    public float reloadTime = 2f;
    public float ammoCapacity = 30;
    public float loadedAmmo = 0;

    //initialise animator and audiosource
    void Start()
    {
        anim = GetComponent<Animator>();

        audio = GetComponent<AudioSource>();

        target = GameObject.Find("Hero").transform;

        //weapon = GameObject.Find("ZombieGun").GetComponent<Transform>();

        loadedAmmo = ammoCapacity;
    }

    //when player is in sight, fire gun when firedelay is elapsed.
    void FixedUpdate()
    {
        //add slow mo effect to audio when slow mo is active
        audio.pitch = Time.timeScale;

        if (!isDead)
        {
            if (spottedHero)
            {
                Vector3 difference = target.position - transform.position;
                difference.Normalize();

                float rot2 = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

                Quaternion newRot = Quaternion.Euler(new Vector3(0.0f, 0.0f, rot2 + adjustmentAngle));

                transform.rotation = Quaternion.Lerp(transform.rotation, newRot, Time.deltaTime * smoothing);
            }

            if (spottedHero)
            {
                if (canFire)
                {
                    if (loadedAmmo > 0)
                    {
                        FireGun();
                    }
                    else
                    {
                        ReloadGun();
                    }
                }
            }
        }
    }

    //plays gun animation and sfx
    //gets machinegunbullet object and instantiates in front of itself
    //sets a delay until next bullet can fire
    void FireGun()
    {
        canFire = false;

        anim.SetTrigger("WeaponFired");

        loadedAmmo--;

        float gunVolume;

        if (!GameManager.instance.heroDead)
        {
            gunVolume = 0.4f;
        }
        else
        {
            gunVolume = 0.1f;
        }

        audio.PlayOneShot(gunshotSounds[Random.Range(0, gunshotSounds.Length)], gunVolume);

        if (playAttackSounds)
        {
            PlayAttackSounds();
        }

        GameObject bullet = PoolManager.instance.GetPooledObject("MGBullet");
        if (bullet != null)
        {
            bullet.transform.position = weapon.transform.position;
            Quaternion randomSpread = Quaternion.Euler(new Vector3(0, 0, Random.Range(-10f, 10f)));
            bullet.transform.rotation = weapon.transform.rotation * randomSpread;
            bullet.SetActive(true);
        }

        Invoke("SetFiring", fireDelay);
    }

    
    void ReloadGun()
    {
        canFire = false;
        loadedAmmo = ammoCapacity;

        audio.PlayOneShot(reloadSound, 0.4f);

        Invoke("SetFiring", reloadTime);
    }

    //end firing delay
    void SetFiring()
    {
        canFire = true;
    }

    //end attack sound delay
    void SetAttackSounds()
    {
        playAttackSounds = true;
    }

    //sets volume of attack sounds lower if hero is dead
    //plays attack sounds 
    private void PlayAttackSounds()
    {
        playAttackSounds = false;
        Invoke("SetAttackSounds", 2f);

        float attackVolume;

        if (!GameManager.instance.heroDead)
        {
            attackVolume = 0.4f;
        }
        else
        {
            attackVolume = 0.1f;
        }

        if (!audio.isPlaying)
        {
            audio.PlayOneShot(attackSounds[Random.Range(0, attackSounds.Length)], attackVolume);
        }
    }

    //(if the zombie isn't already dead)
    //remove the damage dealt from the zombie's health
    //zombie spots hero
    //create a blood splatter object
    //play damage sound effects
    //(if the zombie runs out of health)
    //spill up to four blood splatters
    //create blood spray animated object
    //play death animation
    public void TakeDamage(int damage)
    {
        if (!isDead)
        {
            health -= damage;

            if (!audio.isPlaying)
            {
                audio.PlayOneShot(hitSounds[Random.Range(0, hitSounds.Length)], 0.4f);
            }
            audio.PlayOneShot(squelchSounds[Random.Range(0, squelchSounds.Length)], 0.4f);

            DetectHero();

            SpillBlood();

            if (health <= 0)
            {
                for (int i = 0; i < Random.Range(2, 4); i++)
                {
                    SpillBlood();
                }

                isDead = true;

                //slow time for half a second to make death more satisfying
                Time.timeScale = 0.25f;
                Invoke("ResetTimeScale", 0.09f);

                anim.SetBool("isDead", true);

                Instantiate(bloodsprayPrefab, transform.position, Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + 90f));

                GetComponent<Rigidbody2D>().isKinematic = true;
                GetComponent<Collider2D>().enabled = false;
                GetComponent<SpriteRenderer>().sortingLayerName = "Debris";
            }
        }
    }

    //resets time scale to normal speed
    void ResetTimeScale()
    {
        Time.timeScale = 1;
    }

    //detects the hero
    //plays groan sound
    public void DetectHero()
    {
        if (spottedHero == false)
        {
            spottedHero = true;
            GameManager.instance.unnoticed = false;

            if (!audio.isPlaying)
            {
                audio.PlayOneShot(groanSounds[Random.Range(0, groanSounds.Length)], 0.4f);
            }
        }
    }

    //creates a bloodsplatter sprite near zombie
    private void SpillBlood()
    {
        //if there's an empty bloodsplatter object in pool, set it's location to the zombie with a random rotation
        GameObject splatter = PoolManager.instance.GetPooledObject("Bloodsplatter");
        if (splatter != null)
        {
            Vector3 newPos = new Vector3((gameObject.transform.position.x + Random.Range(-0.45f, 0.45f)), (gameObject.transform.position.y + Random.Range(-0.45f, 0.45f)), 0);
            splatter.transform.position = newPos;
            splatter.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            splatter.SetActive(true);
        }
    }
}
