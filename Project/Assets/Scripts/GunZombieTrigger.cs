﻿using UnityEngine;
using System.Collections;

public class GunZombieTrigger : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hero"))
        {
            AlertZombie();
            gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }

    void AlertZombie()
    {
        GameObject.Find("GunZombie").GetComponent<GunZombieBehaviour>().DetectHero();
    }
}
