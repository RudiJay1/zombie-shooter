﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    public GameObject chance1, chance2, chance3; //the sprite objects for the chances
    public GameObject chancesText;  //text object for the "Chances:" above the chances
    public GameObject noChanceText; //text object for the "NO CHANCE" message upon being bitten

    public GameObject bloodOverlay; //the overlay that appears when the player is damaged

    public GameObject bittenOverlay; //the overlay that appears when the player has been bitten
    public GameObject bittenText; //the label that appears when the player has been bitten

    public GameObject gameOverOverlay; //the overlay that appears when the player is dead
    public GameObject gameOverText; //text object for the "GAME OVER" message upon death

    public GameObject retryButton; //the button that appears to restart the level on death

    public GameObject ammoText; //the game object displaying text

    public Text instructionText; //the text that appears to instruct the player

    private bool showingBloodOverlay = false; //whether the blood overlay should be showing after the player has been damaged


    //initialise health UI with 3 chances
    //set bloodOverlay off by default
    void Start()
    {
        chance1.SetActive(true);
        chance2.SetActive(true);
        chance3.SetActive(true);
        chancesText.SetActive(true);
        noChanceText.SetActive(false);

        bloodOverlay.SetActive(false);

        bittenOverlay.SetActive(false);
        bittenText.SetActive(false);

        gameOverOverlay.SetActive(false);
        gameOverText.SetActive(false);

        retryButton.SetActive(false);

        instructionText.enabled = false;
    }
    
    //change visible UI as the player loses chances
    //make the bitten HUD visible when the player is bitten
    //make game over screen visible when the player is dead
    //turn off game UI when results screen is active
    //only tell the player to reload if they have ammo left
    void Update ()
    {
        if (!GameManager.instance.heroDead)
        {
            //turn off game most UI if showing results screen
            if (GameManager.instance.showingResultsScreen)
            {
                chance1.SetActive(false);
                chance2.SetActive(false);
                chance3.SetActive(false);
                chancesText.SetActive(false);
                noChanceText.SetActive(false);
                bittenText.SetActive(false);
                ammoText.SetActive(false);
                instructionText.enabled = false;
            }
            else
            {
                //if the player has 3 chances
                if (GameManager.instance.chances >= 3)
                {
                    chance1.SetActive(true);
                    chance2.SetActive(true);
                    chance3.SetActive(true);
                    chancesText.SetActive(true);
                    noChanceText.SetActive(false);
                }
                //if the player has 2 chances
                if (GameManager.instance.chances < 3 && GameManager.instance.chances >= 2)
                {
                    chance3.SetActive(false);
                    chance2.SetActive(true);
                    chance1.SetActive(true);
                    chancesText.SetActive(true);
                    noChanceText.SetActive(false);
                }
                //if the player has 1 chance
                else if (GameManager.instance.chances < 2 && GameManager.instance.chances >= 1)
                {
                    chance3.SetActive(false);
                    chance2.SetActive(false);
                    chance1.SetActive(true);
                    chancesText.SetActive(true);
                    noChanceText.SetActive(false);
                }
                //if the player runs out of chances
                else if (GameManager.instance.chances < 1 && GameManager.instance.chances >= 0)
                {
                    chance3.SetActive(false);
                    chance2.SetActive(false);
                    chance1.SetActive(false);
                    chancesText.SetActive(false);
                    noChanceText.SetActive(true);
                }

                //if the player has just been hit show the overlay one time and then invoke turning it off
                if (GameManager.instance.invincible)
                {
                    if (!showingBloodOverlay)
                    {
                        bloodOverlay.SetActive(true);

                        showingBloodOverlay = true;
                        Invoke("RemoveBloodOverlay", 0.1f);
                    }
                }
                //once the player is no longer invincible allow the blood overlay to appear again
                else
                {
                    showingBloodOverlay = false;
                }

                //if the hero is bitten, show the relevant UI and instruction message whilst the player still has ammo
                if (GameManager.instance.heroBitten)
                {
                    bittenOverlay.SetActive(true);
                    bittenText.SetActive(true);
                    if (GameManager.instance.loadedAmmo > 0)
                    {
                        instructionText.enabled = true;
                        instructionText.text = "RELOAD TO END IT YOURSELF";
                    }
                    else
                    {
                        instructionText.enabled = true;
                        instructionText.text = "YOU'RE ONLY DELAYING THE INEVITABLE";
                    }
                }
            }
        }
        //if the player is dead, remove all other UI and invoke gameover UI
        else
        {
            chance1.SetActive(false);
            chance2.SetActive(false);
            chance3.SetActive(false);
            chancesText.SetActive(false);
            noChanceText.SetActive(false);

            bittenOverlay.SetActive(false);
            bittenText.SetActive(false);
            instructionText.enabled = false;

            ammoText.SetActive(false);

            Invoke("FadeGameOver", 0.56f);
        }
	}

    //turns off the blood overlay after it has flashed on
    void RemoveBloodOverlay()
    {
        bloodOverlay.SetActive(false);
    }

    //fade in gameover UI
    void FadeGameOver()
    {
        gameOverOverlay.SetActive(true);
        gameOverText.SetActive(true);

        retryButton.SetActive(true);

        gameOverText.GetComponent<Image>().CrossFadeAlpha(255, 3.5f, true);
        gameOverOverlay.GetComponent<Image>().CrossFadeAlpha(175, 3.5f, true);
    }
}
