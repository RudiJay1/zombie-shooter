﻿using UnityEngine;
using System.Collections;

public class HeroHealth : MonoBehaviour {

    public float speedDecay = 0.7f; //how much hero movement speed decreases by when damage is taken

    private AudioSource audio; //audio source
    public AudioClip[] hitSounds; //array to hold damage grunt sound effects
    public AudioClip[] squelchSounds; //array to hold squelch sound effects
    public AudioClip synthSound; //sound effect to play on death

    //initialise audiosource
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    //if the player isn't dead but has no lives, make them bitten
    //if they are dead, make them not bitten
    void Update ()
    {
        //add slow mo effect to audio when slow mo is active
        audio.pitch = Time.timeScale;

        //if live count is less than or equal to zero and the player isn't bitten or dead already, set them to bitten
        if (GameManager.instance.heroDead)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                GameManager.instance.SendMessage("RestartLevel");
            }
        }
	}

    //if the player is dead, take away control and make the player no longer bitten
    //play low pitch sound effect and slow time
    //spill a bunch o blood
    //play animation
    //make legs invisible as they are replaced by death animation
    //set the hero to dead
    void PlayerDead()
    {
        GameManager.instance.allowHeroControl = false;
        GameManager.instance.heroBitten = false;

        GetComponent<Animator>().SetTrigger("Dead");

        audio.PlayOneShot(synthSound, 0.4f);

        Time.timeScale = 0.25f;        
        audio.pitch = Time.timeScale;

        for (int i = 0; i < Random.Range(2, 4); i++)
        {
            Invoke("SpillBlood", 0.6f);
        }

        transform.FindChild("Legs").gameObject.SetActive(false);

        GameManager.instance.heroDead = true;
    }

    //(if the hero isn't already dead)
    //if hero takes damage but isn't invincible, if they are bitten they die and game is over
    //if they aren't bitten they become invincible, lose a life, and decrease in movement speed
    //(plays damage shout sound effects)
    //if hero isn't invincible, spill a little blood and shake the screen
    public void TakeDamage(string method)
    {
        if (!GameManager.instance.heroDead)
        {
            if (!GameManager.instance.invincible)
            {
                audio.PlayOneShot(hitSounds[Random.Range(0, hitSounds.Length)], 0.4f);
                audio.PlayOneShot(squelchSounds[Random.Range(0, squelchSounds.Length)], 0.4f);

                SpillBlood();

                GameManager.instance.shakeScreen = true;

                if (GameManager.instance.heroBitten)
                {
                    PlayerDead();
                }
                else
                {
                    GameManager.instance.invincible = true;
                    Invoke("ToggleInvincible", GameManager.instance.invincibilityTime);

                    GameManager.instance.chances--;

                    if (GameManager.instance.heroSpeed > 1)
                    {
                        GameManager.instance.heroSpeed -= speedDecay;
                    }

                    if (GameManager.instance.chances <= 0)
                    {
                        if (method == "bite")
                        {
                            GameManager.instance.heroBitten = true;
                        }
                        else if (method == "bullet")
                        {
                            PlayerDead();
                        }
                    }
                }
            }
        }
    }

    //once invincibility runs out, turn it off
    public void ToggleInvincible()
    {
        GameManager.instance.invincible = false;
    }

    //create bloodsplatter sprite near hero when damaged
    void SpillBlood()
    {
        //if there's an empty bloodsplatter object in pool, set it's location to the hero with a random rotation
        GameObject splatter = PoolManager.instance.GetPooledObject("Bloodsplatter");
        if (splatter != null)
        {
            Vector3 newPos = new Vector3((gameObject.transform.position.x + Random.Range(-0.45f, 0.45f)), (gameObject.transform.position.y + Random.Range(-0.45f, 0.45f)), 0);
            splatter.transform.position = newPos;
            splatter.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            splatter.SetActive(true);
        }
    }

}
