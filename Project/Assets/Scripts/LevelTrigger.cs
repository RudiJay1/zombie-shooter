﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelTrigger : MonoBehaviour {

    public bool clearEnemiesPossible = true;
    public bool unnoticedPossible = true;

    public int bestChances = 0;
    public float bestTime = 0;
    public int bestAmmo = 0;
    public int enemiesClearedBonus = 0;
    public int unnoticedBonus = 0;

    private float timeTaken = 0;

    //the gameobjects representing various UI elements turned on and off by this script
    public GameObject resultsScreen, //the gameobject containing the screen itself
        timeTakenLabel, //the label for time taken
        chanceRemainingLabel, chanceBittenText, chanceIcons, //the label for chances remaining, the icons to display the number and "BITTEN" text
        ammoSavedLabel,  //the label  for ammo saved
        stageResultLabel; //the label for overall stage performance

    public Text timeResult, ammoResult, stageResult, //the text objects to display the results
        enemiesResultText, noticedResultText; //the labels for "enemies cleared" and "unnoticed" bonuses 
    
    void Start()
    {
        enemiesResultText.enabled = false;
        noticedResultText.enabled = false;
    }

    //when the hero enters the level goal trigger, start slowing time
    //turn off level exit collider so that time does not slow down again once results screen is left
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hero"))
        {
            InvokeRepeating("SlowTime", 0, 0.1f);
            gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }

    //slow down time if it has yet to completely stop and call results screen
    void SlowTime()
    {
        if (Time.timeScale > 0)
        {
            Time.timeScale -= 0.25f;
        }
        ShowResultsScreen();
    }

    //once time is nearly frozen, bring time to a stop
    //make the results screen UI visible and other UI invisible
    //show results
    //wait for input to start the next level or exit to the main menu
    void ShowResultsScreen()
    {
        if (Time.timeScale < 0.1f)
        {
            GameManager.instance.shakeScreen = false;
            GameManager.instance.allowHeroControl = false;
            GameManager.instance.showingResultsScreen = true;

            CancelInvoke("SlowTime");
            Time.timeScale = 0;

            resultsScreen.SetActive(true);

            chanceRemainingLabel.SetActive(true);
            if (GameManager.instance.heroBitten)
            {
                chanceBittenText.SetActive(true);
            }
            else
            {
                if (GameManager.instance.chances == 3)
                {
                    chanceIcons.transform.FindChild("Chance2").gameObject.SetActive(true);
                    chanceIcons.transform.FindChild("Chance1").gameObject.SetActive(true);
                    chanceIcons.transform.FindChild("Chance3").gameObject.SetActive(true);
                }
                else if (GameManager.instance.chances >= 2 && GameManager.instance.chances < 3)
                {
                    chanceIcons.transform.FindChild("Chance2").gameObject.SetActive(true);
                    chanceIcons.transform.FindChild("Chance1").gameObject.SetActive(true);
                }
                else if (GameManager.instance.chances >= 1 && GameManager.instance.chances < 2)
                {
                    chanceIcons.transform.FindChild("Chance2").gameObject.SetActive(true);
                }
            }

            //set time labels active and calculate time taken
            timeTakenLabel.SetActive(true);
            timeResult.text = CalculateTimeResult();
            timeResult.enabled = true;

            ammoSavedLabel.SetActive(true);
            ammoResult.text = (GameManager.instance.loadedAmmo + GameManager.instance.storedAmmo).ToString();
            ammoResult.enabled = true;

            //if it's possible to clear all enemies in the level, show greyed out element on results screen
            //check to see if enemies are cleared and if so set text colour to white
            if (clearEnemiesPossible)
            {
                CheckEnemiesCleared();
                if (GameManager.instance.enemiesCleared)
                {
                    enemiesResultText.color = new Color(255, 255, 255);
                }
                enemiesResultText.enabled = true;
            }

            //if it's possible to go unnoticed in the level, show the greyed out element on results screen
            //if the player is unnoticed set text color to white
            if (unnoticedPossible)
            {
                if (GameManager.instance.unnoticed)
                {
                    noticedResultText.color = new Color(255, 255, 255);
                }
                noticedResultText.enabled = true;
            }
            else
            {
                noticedResultText.enabled = false;
            }

            stageResultLabel.SetActive(true);
            stageResult.text = CalculateStageResult();
            stageResult.enabled = true;
        }
    }

    //calculate the time taken to complete a stage
    //convert the time taken into "mm:ss" string format
    string CalculateTimeResult()
    {
        float time = Time.realtimeSinceStartup - GameManager.instance.startTime
            + GameManager.instance.timeBeforePause;

        timeTaken = time;

        string minutes = Mathf.Floor(time / 60).ToString("00");
        string seconds = Mathf.Floor(time % 60).ToString("00");

        string result = minutes + ":" + seconds;

        return result;
    }

    //go through all enemy gameobject and check to see if they are all dead
    void CheckEnemiesCleared()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Zombie");

        //assume enemies cleared is true
        GameManager.instance.enemiesCleared = true;

        //go through all enemies and if one is dead then enemies cleared is not true
        foreach (GameObject enemy in enemies)
        {
            if (enemy.GetComponent<ZombieBehaviour>() != null)
            {
                if (!enemy.GetComponent<ZombieBehaviour>().isDead)
                {
                    GameManager.instance.enemiesCleared = false;
                    break;
                }
            }
            else if (enemy.GetComponent<GunZombieBehaviour>() != null)
            {
                if (!enemy.GetComponent<GunZombieBehaviour>().isDead)
                {
                    GameManager.instance.enemiesCleared = false;
                    break;
                }
            }
        }
    }

    //compares player's chances, time taken and ammo saved to the values set by the dev for that level
    //gets a score out 100 for all three
    //add the bonus points if the player qualifies for them
    //determine the player's grade based on their score and returns to UI.
    string CalculateStageResult()
    {
        string result = "";

        float score = 0;

        score += (GameManager.instance.chances * 100) / bestChances;
        score += ((bestTime * 100) / timeTaken);
        score += ((GameManager.instance.loadedAmmo + GameManager.instance.storedAmmo) * 100) / bestAmmo;

        if (GameManager.instance.enemiesCleared)
        {
            score += enemiesClearedBonus;
        }
        if (GameManager.instance.unnoticed)
        {
            score += unnoticedBonus;
        }

        if (score >= 300)
        {
            result = "S";
        }
        else if (score >= 260 && score < 300)
        {
            result = "A";
        }
        else if (score >= 220 && score < 260)
        {
            result = "B";
        }
        else if (score >= 180 && score < 220)
        {
            result = "C";
        }
        else if (score >= 140 && score < 180)
        {
            result = "D";
        }
        else if (score < 140)
        {
            result = "E";
        }

        return result;
    }

}
