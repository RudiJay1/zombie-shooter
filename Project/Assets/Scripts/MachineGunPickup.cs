﻿using UnityEngine;
using System.Collections;

public class MachineGunPickup : MonoBehaviour {

    private AudioSource audio; //audio source
    public AudioClip reloadSound; //reload sound effect

    bool pickedUp = false;

    //initialis audiosource
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    
    //when player triggers this object, calls gamemanager function to give player better gun
    //plays reload sound
    //removes itself
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hero"))
        {
            if (!pickedUp)
            {
                pickedUp = true;

                audio.PlayOneShot(reloadSound, 0.4f);

                GameManager.instance.PickUpFullAuto();

                gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }
}
