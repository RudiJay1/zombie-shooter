﻿using UnityEngine;
using System.Collections;

public class MouseLook2D : MonoBehaviour {

    private Camera theCamera; //the game camera

    public float smoothing = 5.0f; //how smoothly the hero will turn to face the mouse cursor
    public float adjustmentAngle = 0.0f; //the angle to rotate the hero sprite so that it follows the mouse cursor

    public float facingAngle = 0.0f; //the angle the character is facing towards

    //initialises the game camera
    void Start()
    {
        theCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

	//(if the hero isn't dead)
    //rotates hero towards mouse cursor
	void Update ()
    {
        //get the position of the mouse relative to the camera
        Vector3 target = theCamera.ScreenToWorldPoint(Input.mousePosition);
        //get the difference between the current position of hero and mouse position
        Vector3 difference = target - transform.position;

        //normalise the difference
        difference.Normalize();

        //get the angle of the mouse position from base point
        facingAngle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        //get the amount to rotate the hero to face mouse position
        Quaternion newRotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, facingAngle + adjustmentAngle));

        //(if the player has control)
        //rotate hero towards mouse cursor
        if (GameManager.instance.allowHeroControl)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * smoothing);
        }
	}
}
