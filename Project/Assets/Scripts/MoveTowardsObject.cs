﻿using UnityEngine;
using System.Collections;

public class MoveTowardsObject : MonoBehaviour {

    private Rigidbody2D rb2d; //the rigidbody of the object
    private Collider2D col2d; //the colliders of the object
    private SpriteRenderer sr; //the sprite renderer of the object
    private ZombieBehaviour behaviour; //the zombie behaviour class to get isDead variable

    public float speed = 5.0f; //speed the object is moving at

    //instantiate components
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        col2d = GetComponent<Collider2D>();
        sr = GetComponent<SpriteRenderer>();
        behaviour = GetComponent<ZombieBehaviour>();
    }

    //(if the zombie isn't dead)
    //if there is a target, move the rigidbody towards it at speed
	void FixedUpdate ()
    {
        if (!behaviour.isDead)
        {
            if (behaviour.spottedHero)
            {
                rb2d.velocity = Vector2.zero;
                rb2d.angularVelocity = 0.0f;

                transform.position = Vector3.MoveTowards(transform.position, behaviour.target.position, speed * 0.01f);
            }
        }
        //(if the zombie is dead)
        //make the zombie immoveable and allow it to be walked over
        //place the zombie at a lower sorting layer so the other objects are placed over it
        else
        {
            rb2d.isKinematic = true;
            col2d.enabled = false;
            sr.sortingLayerName = "Debris";
        }

	}
}
