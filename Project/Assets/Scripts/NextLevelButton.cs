﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NextLevelButton : MonoBehaviour {

    public GameObject succumbCanvas; //the background to show when the player succumbs to the bite
    public GameObject retryButton; //retry button that appears on succumb screen

    //when the player clicks the continue button on the results screen
    //if they are not bitten, they go to the next level
    //if they are bitten, remove all UI on screen and show the "turning into a zombie" screen and animation
    //wait for space bar before reloading level
	public void ClickedNextLevelButton()
    {
        
        if (GameManager.instance.heroBitten)
        {
            //set time to normal so animation works
            Time.timeScale = 1;

            //disable all UI elements
            GameObject canvas = GameObject.Find("HUD Canvas");
            for (int i = 0; i < canvas.transform.childCount; i++)
            {
                GameObject child = canvas.transform.GetChild(i).gameObject;
                if (child!= null)
                {
                    child.SetActive(false);
                }
            }

            succumbCanvas.SetActive(true);
            retryButton.SetActive(true);
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }
    }
}
