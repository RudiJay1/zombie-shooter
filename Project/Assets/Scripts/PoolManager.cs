﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour {

    public static PoolManager instance; //the pool manager object

    public string[] names; //the array storing the names of the pooled objects
    public GameObject[] pooledObjects; //array storing the pooled objects
    public int[] poolAmounts; //array storing the number of instances of pooled objects to pool

    public Hashtable mainPool = new Hashtable(); //hashtable to hold all the instances of objects

    public List<GameObject> tempList; //list to hold object instances whilst game is running

    //instantiate instance of pool manager
    void Awake()
    {
        instance = this;
    }

    //initialise object pool
    void Start()
    {
        tempList = new List<GameObject>();

        for (int i = 0; i < names.Length; i++)
        {
            List<GameObject> objList = new List<GameObject>();

            for (int j = 0; j < poolAmounts[i]; j++)
            {
                GameObject obj = (GameObject)Instantiate(pooledObjects[i]);
                obj.transform.parent = transform;
                obj.SetActive(false);
                objList.Add(obj);
            }

            mainPool.Add(names[i], objList);
        }
    }

    //return the first instance of a pooled object using name
    public GameObject GetPooledObject(string name)
    {
        if (mainPool.ContainsKey(name))
        {
            tempList = mainPool[name] as List<GameObject>;

            for (int i = 0; i < tempList.Count; i++)
            {
                if (tempList[i] != null)
                {
                    if (!tempList[i].activeInHierarchy)
                    {
                        return tempList[i];
                    }
                }
            }
        }

        return null;
    }

    //disable all the pooled objects
    public void ResetPool()
    {
        for (int i = 0; i < tempList.Count; i++)
        {
            if (tempList[i] != null)
            {
                if (tempList[i].activeInHierarchy)
                {
                    tempList[i].SetActive(false);
                }
            }
        }
    }


}
