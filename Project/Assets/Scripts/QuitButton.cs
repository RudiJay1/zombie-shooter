﻿using UnityEngine;
using System.Collections;

public class QuitButton : MonoBehaviour {

    //quit the program
	public void ClickQuit()
    {
        Application.Quit();
    }

}
