﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class RetryButton : MonoBehaviour {

    //retries the current level when button is clicked
	public void ClickedRetryButton()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
