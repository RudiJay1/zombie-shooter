﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour {

    public Camera camera;
    public float shakeDuration = 0; //the time remaining to shake the screen
    public float fullShakeDuration = 1; //the maximum duration to shake the screen for
    public float shakeAmount = 0.5f; //how much to shake the object by
 
    //if told to shake the screen, set a duration to shake the screen for
    //shake the screen and decrement the duration to shake the screen for
    //stop shaking the screen when results screen is showing
    //once the duration is up, stop shaking screen
    void Update()
    {
        if (GameManager.instance.shakeScreen)
        {
            shakeDuration = fullShakeDuration;
            GameManager.instance.shakeScreen = false;
        }

        if (shakeDuration > 0 && !GameManager.instance.showingResultsScreen)
        {
            camera.transform.localPosition += Random.insideUnitSphere * shakeAmount;
            shakeDuration -= Time.deltaTime;
        }
        else
        {
            shakeDuration = 0.0f;
        }
    }
}
