﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShootBullet : MonoBehaviour
{

    public GameObject weapon; //the weapon the hero is carrying

    private Animator heroAnim; //the hero animator

    private AudioSource audio;

    public AudioClip[] gunshotSounds; //array to hold gunshot audio in
    public AudioClip[] emptyClipSounds; //array to hold empty clip audio in
    public AudioClip reloadSound; //reload audio

    public GameObject bloodsprayPrefab; //blood spray object that appears on suicide

    private bool isFiring = false; //whether the hero is currently firing or not
    private bool isReloading = false; //whether the hero is currently reloading or not

    //initialise animator
    //initialise weapon to false at startup
    void Start()
    {
        heroAnim = GetComponent<Animator>();

        audio = GetComponent<AudioSource>();

        weapon.SetActive(false);
    }

    //allows the hero to fire and reload again
    void SetFiring ()
    {
        isFiring = false;
        isReloading = false;
	}

    //stops the hero from being able to fire or reload
    //calls function to make zombies in detection range hear gun
    //triggers the pistol firing animation
    //plays gunshot sound effect
    //turns on screenshake
    //sets the location of the bullet,
    //starts delay before the hero can fire or reload again
    void Fire()
    {
        isFiring = true;
        isReloading = true;

        MakeNoise();

        GameManager.instance.loadedAmmo--;

        heroAnim.SetTrigger("WeaponFired");

        audio.PlayOneShot(gunshotSounds[Random.Range(0, gunshotSounds.Length)], 0.6f);

        GameManager.instance.shakeScreen = true;

        GameObject bullet = PoolManager.instance.GetPooledObject("Bullet");
        if (bullet != null)
        {
            bullet.transform.position = weapon.transform.position;
            Quaternion randomSpread = Quaternion.Euler(new Vector3(0, 0, Random.Range(-GameManager.instance.spread, GameManager.instance.spread)));
            bullet.transform.rotation = weapon.transform.rotation * randomSpread;
            bullet.SetActive(true);
        }

        Invoke("SetFiring", GameManager.instance.fireDelay);
    }

    //reloads the current weapon with ammo store, then allows the hero to reload and fire again
    void Reload()
    {
        //how much ammo to load into weapon on reload
        int ammoToLoad = GameManager.instance.ammoCapacity - GameManager.instance.loadedAmmo;

        //if there isn't enough stored ammo to fully load, just use whatever is available
        if (ammoToLoad > GameManager.instance.storedAmmo)
        {
            ammoToLoad = GameManager.instance.storedAmmo;
        }

        //take the loaded ammo out of the store
        GameManager.instance.storedAmmo -= ammoToLoad;

        //load it into the weapon
        GameManager.instance.loadedAmmo += ammoToLoad;

        SetFiring();
    }

    //when the hero fires a gun, alert all enemies in detection range
    void MakeNoise()
    {
        Collider2D[] nearbyEnemies = Physics2D.OverlapCircleAll(gameObject.transform.position, GameManager.instance.gunNoiseRadius);

        foreach (Collider2D enemy in nearbyEnemies)
        {
            if (enemy.CompareTag("Zombie"))
            {
                enemy.SendMessage("DetectHero");
            }
        }
    }

    //animate player hero shooting self
    //spray blood
    //make the player dead
    void EndSelf()
    {
        heroAnim.SetTrigger("EndedSelf");

        Invoke("SprayBlood", 0.5f);

        SendMessage("PlayerDead");
    }

    //sprays blood when player hero ends themselves
    //plays gunshot sound effect
    void SprayBlood()
    {
        audio.PlayOneShot(gunshotSounds[Random.Range(0, gunshotSounds.Length)], 0.5f);
        Instantiate(bloodsprayPrefab, transform.position, Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + 45f));
    }
	
    //(if the player isn't dead)
    //if the player isn't currently reloading 
    //if the player is holding the right mouse button, start aiming weapon
    //if left mouse button is pressed whilst aiming, fire weapon
    //if left mouse button is pressed alone, reload weapon
    //if left mouse button is pressed and hero is bitten, restart level
    void Update()
    {
        //if the player has control
        if (GameManager.instance.allowHeroControl)
        {
            //stop aiming, firing or reloading again whilst player is reloading
            if (!isReloading)
            {
                //if right mouse button is held
                if (Input.GetMouseButton(1))
                {
                    //start aiming weapon
                    heroAnim.SetBool("isAiming", true);
                    weapon.SetActive(true);

                    //if left mouse button is pressed whilst right mouse button is held
                    if (Input.GetMouseButton(0))
                    {
                        //if ammo is remaining
                        if (GameManager.instance.loadedAmmo > 0)
                        {
                            //attempt to fire the weapon
                            if (!isFiring)
                            {
                                Fire();
                            }
                        }
                        //if gun is empty but trigger pulled, play empty clip sound effect
                        else
                        {
                            if (!audio.isPlaying)
                            {
                                audio.PlayOneShot(emptyClipSounds[Random.Range(0, emptyClipSounds.Length)], 0.4f);
                            }
                        }
                    }

                }

                //if the right mouse button isn't held
                else
                {
                    //stop aiming weapon
                    heroAnim.SetBool("isAiming", false);
                    weapon.SetActive(false);

                    //if left mouse button is pressed by itself
                    if (Input.GetMouseButton(0))
                    {
                        //if the hero isn't bitten
                        if (GameManager.instance.heroBitten == false)
                        {
                            //if the hero isn't already reloading and has ammo remaining and clip isn't already full
                            if (GameManager.instance.storedAmmo > 0 && GameManager.instance.loadedAmmo < GameManager.instance.ammoCapacity)
                            {
                                //play gunshot sound effect
                                audio.PlayOneShot(reloadSound, 0.4f);

                                //reload when the reload time has elapsed, stop the player from reloading or firing in the meantime
                                Invoke("Reload", GameManager.instance.reloadTime);

                                heroAnim.SetTrigger("Reloaded");

                                isReloading = true;
                                isFiring = true;
                            }
                        }
                        //if the hero is bitten they shoot themselves (if they have ammo)
                        else
                        {
                            if (GameManager.instance.loadedAmmo > 0)
                            {
                                EndSelf();
                            }
                        }
                    }

                }
            }
        }

        //if the hero is dead, stop them from aiming
        if (GameManager.instance.heroDead)
        {
            heroAnim.SetBool("isAiming", false);
        }

    }
}
