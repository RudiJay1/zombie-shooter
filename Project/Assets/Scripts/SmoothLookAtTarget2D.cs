﻿using UnityEngine;
using System.Collections;

public class SmoothLookAtTarget2D : MonoBehaviour {

    public float smoothing = 5.0f; //the smoothness the zombie rotates towards its target at
    public float adjustmentAngle = 0.0f; //angle to rotate sprite so it is facing correct way

    private ZombieBehaviour behaviour;

    void Start()
    {
        behaviour = GetComponent<ZombieBehaviour>();
    }

    //(if the zombie isn't dead)
    //if there is a target, rotate the sprite until it faces it using smoothing
    void FixedUpdate () 
    {
        if (!behaviour.isDead)
        {
            if (behaviour.spottedHero)
            {
                Vector3 difference = behaviour.target.position - transform.position;
                difference.Normalize();

                float rot2 = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;

                Quaternion newRot = Quaternion.Euler(new Vector3(0.0f, 0.0f, rot2 + adjustmentAngle));

                transform.rotation = Quaternion.Lerp(transform.rotation, newRot, Time.deltaTime * smoothing);
            }
        }
	}
}
