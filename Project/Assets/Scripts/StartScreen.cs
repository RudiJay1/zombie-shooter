﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {

    private AudioSource audio; //audiosource
    public AudioClip titleSound; //sound clip of title narration
    public AudioClip synthSound; //sound clip to play when key is pressed 

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

	//checks for input, plays synth sound and invokes title narration
    void Update ()
    {
	    if (Input.anyKeyDown)
        {
            if (!audio.isPlaying)
            {
                audio.PlayOneShot(synthSound, 0.4f);
                Invoke("PlayNarration", 3.5f);
            }
        }
	}

    //plays title narration and invokes Main Menu load
    void PlayNarration()
    {
        audio.PlayOneShot(titleSound, 0.4f);
        Invoke("LoadMenu", 2.5f);
    }

    //loads the main menu
    void LoadMenu()
    {
        Application.LoadLevel("MainMenu");
    }
}
