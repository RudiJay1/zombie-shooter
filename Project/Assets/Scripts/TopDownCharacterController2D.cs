﻿using UnityEngine;
using System.Collections;

public class TopDownCharacterController2D : MonoBehaviour {

    private Rigidbody2D rb2d;    //the character rigidbody
    private MouseLook2D mouseLook; //the character rotator
    private Animator legAnim; //the animator for the hero's legs

    private AudioSource audio; //audio source

    //initialise components
	void Start ()
    {
        rb2d = GetComponent<Rigidbody2D>();
        mouseLook = GetComponent<MouseLook2D>();
        audio = GameObject.Find("Hero/Legs").GetComponent<AudioSource>();

        legAnim = GameObject.Find("Hero/Legs").GetComponent<Animator>();
	}

    //(if the hero isn't dead)
    //moves hero when movement keys are pressed based
    //gets difference between angle hero is facing and angle hero is moving
    //plays footstep sound effect when moving
    //animates legs of hero based on what way they are facing
	void FixedUpdate ()
    {
            //the amount the player is pressing on the horizontal and vertical movement keys
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            //the angle the player is moving the hero in
            float movementAngle = Mathf.Atan2(y, x) * Mathf.Rad2Deg;

            //whether the player is moving backwards or forwards
            bool runningForwards = false;
            bool runningBackwards = false;
                    
            //if the player is moving
            if (x != 0 || y != 0)
            {
                audio.pitch = Time.timeScale;
                //start playing footstep sfx
                if (!audio.isPlaying && !GameManager.instance.showingResultsScreen)
                {
                    audio.Play();
                }
                //stop audio if the results screen is showing
                if (GameManager.instance.showingResultsScreen)
                {
                    audio.Stop();
                }

                //if the hero is facing in the direction they are moving, animate running forwards
                if ((movementAngle > mouseLook.facingAngle - 90f) && (movementAngle < mouseLook.facingAngle + 90f))
                {
                    runningForwards = true;
                    runningBackwards = false;
                }
                //if the hero is facing away from the direction they are moving, animate running forwards
                else if ((movementAngle > mouseLook.facingAngle + 135f) || (movementAngle < mouseLook.facingAngle - 135f))
                {
                    runningForwards = false;
                    runningBackwards = true;
                }
            }
            //if player stops moving 
            else
            {
                audio.Stop();
            }
            
            //(if the player has control)
            //add velocity to hero based on horizontal and vertical input
            if (GameManager.instance.allowHeroControl)
            {
                rb2d.velocity = new Vector2(x, y) * GameManager.instance.heroSpeed;
                rb2d.angularVelocity = 0.0f;
            }
            //(if the player is dead)
            //make the rigidbody immovable
            //stop legs from animating
            else
            {
                rb2d.isKinematic = true;

                runningForwards = false;
                runningBackwards = false;
            }
            
            //animate legs
            legAnim.SetBool("runningForwards", runningForwards);
            legAnim.SetBool("runningBackwards", runningBackwards);
	}
}
