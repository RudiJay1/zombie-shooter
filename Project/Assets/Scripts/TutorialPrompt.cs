﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialPrompt : MonoBehaviour {

    private Text prompt; //instruction text ui element
    public string tutorial = "";

    private bool shownReloadPrompt = false; //whether the reload tutorial has already been shown
    private bool promptActive = true;

    //find instruction text ui element
    void Start()
    {
        prompt = GameObject.Find("InstructionText").GetComponent<Text>();
    }

    //enable tutorial prompt with given text and remove it after a time
    void ShowPrompt(string text, float time)
    {
        prompt.text = text;
        prompt.enabled = true;

        Invoke("RemovePrompt", time);
    }

    //disable prompt
    void RemovePrompt()
    {
        prompt.enabled = false;
    }

    //if the player enters a tutorial trigger
    //cancel any RemovePrompt invokes
    //show the appropriate tutorial prompt
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hero"))
        {
            CancelInvoke();

            if (tutorial == "Movement")
            {
                ShowPrompt("USE WASD OR ARROW KEYS TO MOVE\nUSE MOUSE CURSOR TO LOOK AROUND", 10f);
            }
            else if (tutorial == "AimShoot")
            {
                ShowPrompt("HOLD RIGHT MOUSE BUTTON TO AIM WEAPON\nPRESS LEFT MOUSE BUTTON WHILE AIMING TO FIRE", 5f);
            }
            else if (tutorial == "Noise")
            {
                ShowPrompt("THE NEARBY DEAD HEAR THE\nSOUND OF YOUR WEAPON", 5f);
            }
            else if (tutorial == "Bitten")
            {
                ShowPrompt("IF YOU ARE BITTEN COMPLETING THE\nLEVEL WILL NOT PROGRESS TO THE NEXT ONE", 5f);
            }
            else if (tutorial == "Stealth")
            {
                ShowPrompt("IN SOME AREAS IT IS POSSIBLE TO SNEAK\nYOUR WAY PAST ALL ENEMIES", 5f);
            }
            else if (tutorial == "MachineGun")
            {
                ShowPrompt("WITH FIREPOWER LIKE THIS YOU CAN TAKE ON AN ARMY", 5f);
            }
        }
    }

    //show tutorial prompts when gameplay parameters are met
    void Update()
    {
        //if the hero is out of ammo and hasn't already been shown the prompt, show tutorial
        if (GameManager.instance.loadedAmmo <= 0)
        {
            if (!shownReloadPrompt)
            {
                ShowPrompt("WHILE NOT AIMING\nPRESS LEFT MOUSE BUTTON TO RELOAD", 5f);
                shownReloadPrompt = true;
            }
        }
        //once the hero has reloaded, allow the prompt to come up again next time
        else
        {
            shownReloadPrompt = false;
        }
    }
}
