﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour {

    private Text ammoText; //text object to display weapon ammo

    //initialise ammo text object
    void Start()
    {
        ammoText = GetComponent<Text>();
    }
	
    //display the current ammo count for the selected weapon
	void Update ()
    {
        if (!GameManager.instance.heroBitten)
        {
            ammoText.text = GameManager.instance.loadedAmmo + " : " + GameManager.instance.storedAmmo;
        }
        else
        {
            ammoText.text = GameManager.instance.loadedAmmo.ToString();
        }
	}
}
