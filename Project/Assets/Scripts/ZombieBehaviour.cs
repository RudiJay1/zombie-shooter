﻿using UnityEngine;
using System.Collections;

public class ZombieBehaviour : MonoBehaviour {

    public Transform target; //the target to rotate towards

    private Animator anim; //the animator for the zombie

    private AudioSource audio; //audio source
    public AudioClip[] attackSounds; //array to hold attack sound effects
    public AudioClip[] groanSounds; //array to hold groan sound effects
    public AudioClip[] hitSounds; //array to hold hit sound effects
    public AudioClip[] squelchSounds; //array to hold squelch sound effects
    private bool playingAttackSounds; //whether or not the zombie is already making attack sounds

    public GameObject bloodsprayPrefab; //the animated sprite for the bloodspray

    public int health = 5;  //number of lives the zombie has

    public float visionAngle = 50; //the angle of the cone the zombie can see the hero within
    public float visionRange = 3.5f; //the distance the zombie can see the hero from

    public bool isDead = false; //whether the zombie is dead again or not
    public bool spottedHero = false; //whether the zombie has seen the hero or not
    public bool distracted = false; //whether the zombie is distracted by an object or not

    //initialise animator and audiosource
    void Start()
    {
        anim = GetComponent<Animator>();

        audio = GetComponent<AudioSource>();

        target = GameObject.Find("Hero").transform;
    }

    //checks whether the zombie is facing in the direction of the hero
    //if so, if the hero is in range, the zombie spots the hero
    void Update()
    {
        //add slow mo effect to audio when slow mo is active
        audio.pitch = Time.timeScale;

        if (!isDead && !distracted)
        {
            Vector3 VectorToHero = target.position - transform.position; //the angle and distance between the hero and the zombie
            Vector3 directionZombieFacing = transform.up;
            float angle = Vector3.Angle(VectorToHero, directionZombieFacing);
            float distance = VectorToHero.magnitude;

            if (angle < visionAngle && distance < visionRange)
            {
                DetectHero();
            }
        }
    }

    //(if the zombie is alive)
    //whilst the zombie is colliding with a living entity, the entity takes damage
    //zombie notices hero in case they hadn't already
    private void OnCollisionStay2D(Collision2D other)
    {
        if (!isDead)
        {
            if (other.gameObject.CompareTag("Hero"))
            {
                other.gameObject.SendMessage("TakeDamage", "bite");
                DetectHero();
            }
        }
    }

    //invokes attacking noises repeatedly
    //if the zombie collides with another that has detected the hero, this zombie detects them too
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!isDead)
        {
            if (other.gameObject.CompareTag("Hero"))
            {
                if (!playingAttackSounds)
                {
                    InvokeRepeating("PlayAttackSounds", 0, 2f);
                }
            }
            else if (other.gameObject.CompareTag("Zombie"))
            {
                if (spottedHero)
                {
                    other.gameObject.SendMessage("DetectHero");
                }
            }
        }        
    }

    //cancels attacking noises when zombie stops contact
    private void OnCollisionExit2D(Collision2D other)
    {
        if (!isDead)
        {
            if (other.gameObject.CompareTag("Hero"))
            {
                CancelInvoke();
                playingAttackSounds = false;
            }
        }
    }

    //sets volume of attack sounds lower if hero is dead
    //plays attack sounds 
    private void PlayAttackSounds()
    {
        float attackVolume;

        if (!GameManager.instance.heroDead)
        {
            attackVolume = 0.4f;
        }
        else
        {
            attackVolume = 0.1f;
        }

        if (!audio.isPlaying)
        {
            audio.PlayOneShot(attackSounds[Random.Range(0, attackSounds.Length)], attackVolume);
        }
    }

    //(if the zombie isn't already dead)
    //remove the damage dealt from the zombie's health
    //zombie spots hero
    //create a blood splatter object
    //play damage sound effects
    //(if the zombie runs out of health)
    //spill up to four blood splatters
    //create blood spray animated object
    //play death animation
    public void TakeDamage(int damage)
    {
        if (!isDead)
        {
            health -= damage;

            if (!audio.isPlaying)
            {
                audio.PlayOneShot(hitSounds[Random.Range(0, hitSounds.Length)], 0.4f);
            }
            audio.PlayOneShot(squelchSounds[Random.Range(0, squelchSounds.Length)], 0.4f);

            DetectHero();

            SpillBlood();

            if (health <= 0)
            {
                for (int i = 0; i < Random.Range(2, 4); i++)
                {
                    SpillBlood();
                }

                isDead = true;

                //slow time for half a second to make death more satisfying
                Time.timeScale = 0.25f;
                Invoke("ResetTimeScale", 0.05f);

                anim.SetBool("isDead", true);

                Instantiate(bloodsprayPrefab, transform.position, Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + 90f));
            }
        }
    }

    //resets time scale to normal speed
    void ResetTimeScale()
    {
        Time.timeScale = 1;
    }

    //detects the hero
    //plays groan sound
    public void DetectHero()
    {
        if (spottedHero == false)
        {
            spottedHero = true;
            GameManager.instance.unnoticed = false;

            if (!audio.isPlaying)
            {
                audio.PlayOneShot(groanSounds[Random.Range(0, groanSounds.Length)], 0.4f);
            }
        }
    }

    //creates a bloodsplatter sprite near zombie
    private void SpillBlood()
    {
        //if there's an empty bloodsplatter object in pool, set it's location to the zombie with a random rotation
        GameObject splatter = PoolManager.instance.GetPooledObject("Bloodsplatter");
        if (splatter != null)
        {
            Vector3 newPos = new Vector3((gameObject.transform.position.x + Random.Range(-0.45f, 0.45f)), (gameObject.transform.position.y + Random.Range(-0.45f, 0.45f)), 0);
            splatter.transform.position = newPos;
            splatter.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
            splatter.SetActive(true);
        }
    }
}
