README
=========

Thank you for looking at my repository for my top-down shooter prototype starring the living and the dead. The game can be found at https://rudijay.itch.io/memories-of-the-dead

Project Details
===

Memories of the Dead is a prototype developed in Unity 5.2.1.

The game features three levels each with their own gimmick. The player progresses through the game in two ways - killing and avoiding the dead. The player can kill the dead by shooting them repeatedly. To do so they must hold the right mouse button to aim and press the left mouse button to fire. To reload they must press the left mouse button without the right mouse button held. In some levels it is possible to kill all the dead, in some it is possible to sneak past them all. If the player fails to do either and gets hit they will lose a chance. Losing three chances will get you bitten and you have no chance to move on to the next level, even if you complete it. Completing the level will give you a score based on your performance.

Running the Project
===

1. Clone the repo.
2. Open Unity 5.2.1 or above.
3. Open the "Project" folder in the repo folder to open the project.

Credits
===

- Code and Art Assets by Rudi Prentice (http://www.rudiprentice.com)

- "TileMaster - 2D Tileset Painter" by Caliber Mengsk (Version 3 (06/08/15)) - Imported from the Unity Asset Store